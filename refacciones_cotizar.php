<!– PARA EJEMPLO DASC — >

<?php
$id_marca_seleccionada = $_GET['marca_id'];
$nombre_marca_seleccionada = $_GET['marca_nombre'];
?>
<!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--código que incluye Bootstrap-->
        <?php include'inc/incluye_bootstrap.php' ?>
        <!--termina código que incluye Bootstrap-->

    </head>
    <body>
        <!--código que incluye el menú responsivo-->
        <?php include'inc/incluye_menu.php' ?>
        <!--termina código que incluye el menú responsivo-->
        <div class="container">
            <div class="jumbotron">

                <form enctype="multipart/form-data" role="form" id="login-form" method="post" class="form-signin" action="cotizacion_guardar.php">

                <form role="form" id="login-form" method="post" class="form-signin" action="cotizacion_guardar.php">
                    <div class="h2">
                        Cotizar con el proveedor
                    </div>

                    <div class="form-group">
                        <label>ID marca seleccionada (<?php echo $nombre_marca_seleccionada ?>)</label>
                        <input type="text" id="marca_id" class="form-control" name="marca_id" value="<?php echo $id_marca_seleccionada ?>" readonly="" 
                               placeholder="<?php echo $nombre_marca_seleccionada ?>">
                    
                    </div>

                    <div class="form-group">
                        <label>Selecciona el proveedor con el que est&aacute;s cotizando (requerido)</label>
                        <input type="text" class="form-control" id="proveedor_id" name="proveedor_id"
                               placeholder="Selecciona al proveedor" style="text-transform:uppercase;" required>
                    </div>
                    <div class="form-group">
                        <label>Fecha de solicitud de precio (requerido)</label>
                        <input type="date" class="form-control" id="fecha_solicitud" name="fecha_solicitud" step="1" 
                               value="<?php echo date("Y-m-d"); ?>" required>
                    </div>
                    <div class="form-group">
                        <label>Precio (requerido)</label>
                        <input type="number" class="form-control" id="precio" name="precio" step="0.01" placeholder="Precio actual" style="text-transform:uppercase;" required>
                    </div>
                    
                    <br>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <input type="reset" class="btn btn-default" value="Limpiar">
                </form>
            </div>
        </div>
    </body>
</html>

