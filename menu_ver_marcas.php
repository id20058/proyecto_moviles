<!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php
        include'inc/incluye_bootstrap.php';
        include 'inc/conexion.php';
        include 'inc/incluye_datatable_head.php';
        ?>
    </head>
    <body>
        <!--código que incluye el menú responsivo-->
        <?php include'inc/incluye_menu.php' ?>
        <!--termina código que incluye el menú responsivo-->
        <div class="container">
            <div class="jumbotron">
                <?php
                //Consulta sin parámetros
                $sel = $con->prepare("SELECT *from marca");

                /* consulta con parametros
                  $sel = $con->prepare("SELECT *from marca WHERE marca_id<=?");
                  $parametro = 50;
                  $sel->bind_param('i', $parametro);
                  finaliza consulta con parámetros */

                $sel->execute();
                $res = $sel->get_result();
                $row = mysqli_num_rows($res);
                ?>
                Elementos devueltos por la consulta: <?php echo $row ?>
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    
                    <tbody>
                        <select name="opciones">
                        <option value="option1">ABARTH</option>
                        <option value="option2">ACURA</option>
                        <option value="option1">ALFA ROMEO</option>
                        <option value="option1">ALPINE</option>
                        <option value="option1">AMERICAN MOTORS</option>
                        <option value="option1">ASIA MOTORS</option>
                        <option value="option2">ASTON MARTIN</option>
                        <option value="option1">AUDI</option>
                        <option value="option1">AUSTIN-HEALEY</option>
                        <option value="option1">BENTLEY</option>
                        <option value="option1">BMW</option>
                        <option value="option2">BUGATTI</option>
                        <option value="option1">BUICK</option>
                        <option value="option1">BYD</option>
                        <option value="option1">CADILLAC</option>
                        <option value="option1">CHERY</option>
                        <option value="option2">CHEVROLET</option>
                        <option value="option1">CHRYSLER</option>
                        <option value="option1">CITROEN</option>
                        <option value="option1">DACIA</option>
                        <option value="option1">DAEWOO</option>
                        <option value="option2">DE TOMASO</option>
                        <option value="option1">DELOREAN</option>
                        <option value="option1">DEVON MOTORWORKS</option>
                        <option value="option1">DFSK</option>
                        <option value="option1">DODGE</option>
                        <option value="option2">DS</option>
                        <option value="option1">FERRARI</option>
                        <option value="option1">FIAT</option>
                        <option value="option1">FORD</option>
                        <option value="option1">GMC</option>
                        <option value="option2">GUMPERT</option>
                        <option value="option1">HONDA</option>
                        <option value="option1">HUMMER</option>
                        <option value="option1">HYUNDAI</option>
                        <option value="option1">INFINITI</option>
                        <option value="option2">INVICTA</option>
                        <option value="option1">ISUZU</option>
                        <option value="option1">JAGUAR</option>
                        <option value="option1">JEEP</option>
                        <option value="option1">KIA</option>
                        <option value="option2">KOENIGSEGG</option>
                        <option value="option1">LADA</option>
                        <option value="option1">LAMBORGHINI</option>
                        <option value="option1">LANCIA</option>
                        <option value="option1">LAND ROVER</option>
                        <option value="option2">LEXUS</option>
                        <option value="option1">LINCOLN</option>
                        <option value="option1">LOTUS</option>
                        <option value="option1">MAHINDRA</option>
                        <option value="option1">MARCOS</option>
                        <option value="option2">MASERATI</option>
                        <option value="option1">MAYBACH</option>
                        <option value="option1">MAZDA</option>
                        <option value="option1">MCLAREN</option>
                        <option value="option1">MERCEDES BENZ</option>
                        <option value="option2">MERCURY</option>
                        <option value="option1">MINI</option>
                        <option value="option1">MITSUBISHI</option>
                        <option value="option1">MORGAN</option>
                        <option value="option1">MORRIS MOTORS</option>
                        <option value="option2">NISSAN</option>
                        <option value="option1">OLDSMOBILE</option>
                        <option value="option1">OPEL</option>
                        <option value="option1">PAGANI</option>
                        <option value="option1">PEUGEOT</option>
                        <option value="option2">PLYMOUTH</option>
                        <option value="option1">PONTIAC</option>
                        <option value="option1">PORSCHE</option>
                        <option value="option1">RENAULT</option>
                        <option value="option1">ROLLS-ROYCE</option>
                        <option value="option2">ROVER</option>
                        <option value="option1">SALEEN</option>
                        <option value="option1">SATURN</option>
                        <option value="option1">SEAT</option>
                        <option value="option1">SHELBY</option>
                        <option value="option2">SKODA</option>
                        <option value="option1">SMART</option>
                        <option value="option1">SPYKER CARS</option>
                        <option value="option1">SSANGYONG</option>
                        <option value="option1">SUBARU</option>
                        <option value="option2">SUZUKI</option>
                        <option value="option1">TATA</option>
                        <option value="option1">TESLA</option>
                        <option value="option1">TOYOTA</option>
                        <option value="option1">VOLKSWAGEN VW</option>
                        <option value="option2">VOLVO</option>
                        <option value="option1">W MOTORS</option>
                        
                        </select>
                                    
                        
                    <tbody>
                </table>
            </div>
        </div>
        <?php
        include 'inc/incluye_datatable_pie.php';
        ?>
    </body>
</html>

